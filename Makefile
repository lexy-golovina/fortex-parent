HOST=10.2.98.25

build:
    docker-compose build

start:
    export HOST="$(HOST)"
    docker-compose up

stop:
    docker-compose stop

run:
    stop build start